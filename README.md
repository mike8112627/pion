
##  Golang WebRTC Pion Video Communication

### Overview
This project aims to enable real-time video communication using WebRTC Pion in Golang. It targets both desktop and IoT devices, with a focus on low-resource ARM-based hardware like the Raspberry Pi. The application captures RTSP streams from IP cameras and relays them to clients via WebRTC.

### Features
- **Selective Forwarding Unit (SFU)**: Implement SFU to handle communication between peers.
- **Socket-Based Protocol**: Communicate with an existing Node.js middleware via a socket-based protocol.
- **Low-Resource Optimization**: Optimize for ARM-based hardware, ensuring efficient resource usage.

### Prerequisites
Before running the application, ensure you have the following:
- **Golang**: Proficiency in Golang programming language.
- **WebRTC Pion**: Familiarity with WebRTC Pion library.
- **Experience with Real-Time Applications**: Hands-on experience in creating real-time applications for both desktop and IoT platforms.

### Installation and Usage
```
cd existing_repo
git remote add origin https://gitlab.com/mike8112627/pion.git
git branch -M main
git push -uf origin main
```
### Troubleshooting
If you encounter connection issues or other problems:
- Check the logs for error messages.
- Verify that your RTSP stream is accessible.
- Ensure proper configuration of the SFU.


