package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/deepch/vdk/av"
	"github.com/deepch/vdk/codec/h264parser"
	"github.com/deepch/vdk/format/rtsp"
	"github.com/gorilla/websocket"
	"github.com/pion/webrtc/v4"
	"github.com/pion/webrtc/v4/pkg/media"
	"io"
	"log"
	"net"
	"net/http"
	_ "net/http/pprof"
	"sync"
	"time"
)

// nolint
var (
	addr     = flag.String("addr", ":8080", "http service address")
	upgrader = websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool { return true },
	}

	// lock for peerConnections and trackLocals
	listLock        sync.RWMutex
	peerConnections []peerConnectionState

	outboundVideoTrack *webrtc.TrackLocalStaticSample
	api                *webrtc.API
	//rtspURL = "rtsp://admin:admin123456@192.168.245.157:8554/profile1"

	trackLocals map[string]*webrtc.TrackLocalStaticRTP
)

type websocketMessage struct {
	Event string `json:"event"`
	Data  string `json:"data"`
	Id    string `json:"id"`
}

type peerConnectionState struct {
	peerConnection *webrtc.PeerConnection
	websocket      *threadSafeWriter
}

func main() {
	go func() {
		log.Println(http.ListenAndServe("localhost:6060", nil))
	}()
	enableICE_TCP()
	log.Println("V 1.0.0")
	// Parse the flags passed to program
	flag.Parse()

	// Init other state
	log.SetFlags(0)

	var err error
	outboundVideoTrack, err = webrtc.NewTrackLocalStaticSample(webrtc.RTPCodecCapability{
		MimeType: "video/h264",
	}, "nemesi-device", "nemesi-device")
	if err != nil {
		panic(err)
	}

	go rtspConsumer()
	// websocket handler
	http.HandleFunc("/websocket", websocketHandler)

	// start HTTP server
	log.Fatal(http.ListenAndServe(*addr, nil)) // nolint:gosec
}

func enableICE_TCP() {
	settingEngine := webrtc.SettingEngine{}

	// Enable support only for TCP ICE candidates.
	settingEngine.SetNetworkTypes([]webrtc.NetworkType{
		webrtc.NetworkTypeTCP4,
		webrtc.NetworkTypeTCP6,
		webrtc.NetworkTypeUDP4,
		webrtc.NetworkTypeUDP6,
	})

	tcpListener, err := net.ListenTCP("tcp", &net.TCPAddr{
		IP:   net.IP{0, 0, 0, 0},
		Port: 8443,
	})
	if err != nil {
		panic(err)
	}

	fmt.Printf("Listening for ICE TCP at %s\n", tcpListener.Addr())

	tcpMux := webrtc.NewICETCPMux(nil, tcpListener, 8)
	settingEngine.SetICETCPMux(tcpMux)

	// Creazione di un MediaEngine.
	mediaEngine := webrtc.MediaEngine{}
	mediaEngine.RegisterDefaultCodecs()

	//api = webrtc.NewAPI(webrtc.WithSettingEngine(settingEngine))

	api = webrtc.NewAPI(webrtc.WithMediaEngine(&mediaEngine), webrtc.WithSettingEngine(settingEngine))

}

const ip = "192.168.245.157" // jcp
//const ip = "192.168.193.157" // xiaomi

// The RTSP URL that will be streamed rtsp://admin:admin123456@ - ip -:8554/profile1"
const rtspURL = "rtsp://admin:admin123456@" + ip + ":8554/profile1"

// Connect to an RTSP URL and pull media.
// Convert H264 to Annex-B, then write to outboundVideoTrack which sends to all PeerConnections
func rtspConsumer() {
	log.Println("rtspConsumer start")

	annexbNALUStartCode := func() []byte { return []byte{0x00, 0x00, 0x00, 0x01} }

	for {
		session, err := rtsp.Dial(rtspURL)
		if err != nil {
			log.Println("RTSP dial error:", err)
			time.Sleep(5 * time.Second)
			continue
		}
		session.RtpKeepAliveTimeout = 10 * time.Second

		codecs, err := session.Streams()
		if err != nil {
			panic(err)
		}
		for i, t := range codecs {
			log.Println("Stream", i, "is of type", t.Type().String())
		}
		if codecs[0].Type() != av.H264 {
			panic("RTSP feed must begin with a H264 codec")
		}
		if len(codecs) != 1 {
			log.Println("Ignoring all but the first stream.")
		}

		var previousTime time.Duration
		for {
			pkt, err := session.ReadPacket()
			if err != nil {
				break
			}

			if pkt.Idx != 0 {
				//audio or other stream, skip it
				continue
			}

			pkt.Data = pkt.Data[4:]

			// For every key-frame pre-pend the SPS and PPS
			if pkt.IsKeyFrame {
				pkt.Data = append(annexbNALUStartCode(), pkt.Data...)
				pkt.Data = append(codecs[0].(h264parser.CodecData).PPS(), pkt.Data...)
				pkt.Data = append(annexbNALUStartCode(), pkt.Data...)
				pkt.Data = append(codecs[0].(h264parser.CodecData).SPS(), pkt.Data...)
				pkt.Data = append(annexbNALUStartCode(), pkt.Data...)
			}

			bufferDuration := pkt.Time - previousTime
			previousTime = pkt.Time
			if err = outboundVideoTrack.WriteSample(media.Sample{Data: pkt.Data, Duration: bufferDuration}); err != nil && err != io.ErrClosedPipe {
				panic(err)
			}
		}

		if err = session.Close(); err != nil {
			log.Println("session Close error", err)
		}

		time.Sleep(5 * time.Second)
	}
}

// Handle incoming websockets
func websocketHandler(w http.ResponseWriter, r *http.Request) {

	id := r.URL.Query().Get("id")

	/*
		eg:
		[{"url":"stun:fr-turn3.xirsys.com"},{"username":"qwmtavWkx5SjxTMXPnMXUcgE34ezMv0rz1dSqi-GRQQBgGsamQfAPsBhdC5IgsWCAAAAAGYUWqtoYWxmcG9ja2V0","url":"turn:fr-turn3.xirsys.com:80?transport=udp","credential":"e06dca48-f5ea-11ee-9de9-0242ac120004"},{"username":"qwmtavWkx5SjxTMXPnMXUcgE34ezMv0rz1dSqi-GRQQBgGsamQfAPsBhdC5IgsWCAAAAAGYUWqtoYWxmcG9ja2V0","url":"turn:fr-turn3.xirsys.com:3478?transport=udp","credential":"e06dca48-f5ea-11ee-9de9-0242ac120004"},{"username":"qwmtavWkx5SjxTMXPnMXUcgE34ezMv0rz1dSqi-GRQQBgGsamQfAPsBhdC5IgsWCAAAAAGYUWqtoYWxmcG9ja2V0","url":"turn:fr-turn3.xirsys.com:80?transport=tcp","credential":"e06dca48-f5ea-11ee-9de9-0242ac120004"},{"username":"qwmtavWkx5SjxTMXPnMXUcgE34ezMv0rz1dSqi-GRQQBgGsamQfAPsBhdC5IgsWCAAAAAGYUWqtoYWxmcG9ja2V0","url":"turn:fr-turn3.xirsys.com:3478?transport=tcp","credential":"e06dca48-f5ea-11ee-9de9-0242ac120004"},{"username":"qwmtavWkx5SjxTMXPnMXUcgE34ezMv0rz1dSqi-GRQQBgGsamQfAPsBhdC5IgsWCAAAAAGYUWqtoYWxmcG9ja2V0","url":"turns:fr-turn3.xirsys.com:443?transport=tcp","credential":"e06dca48-f5ea-11ee-9de9-0242ac120004"},{"username":"qwmtavWkx5SjxTMXPnMXUcgE34ezMv0rz1dSqi-GRQQBgGsamQfAPsBhdC5IgsWCAAAAAGYUWqtoYWxmcG9ja2V0","url":"turns:fr-turn3.xirsys.com:5349?transport=tcp","credential":"e06dca48-f5ea-11ee-9de9-0242ac120004"}]
	*/
	iceServersString := r.URL.Query().Get("iceServers")

	// if epmty set default
	if iceServersString == "" {
		iceServersString = "[{\"urls\": \"stun:stun.l.google.com:19302\"}]"
	}

	/*
		type ICEServer struct {
			URLs           []string          `json:"urls"`
			Username       string            `json:"username,omitempty"`
			Credential     interface{}       `json:"credential,omitempty"`
			CredentialType ICECredentialType `json:"credentialType,omitempty"`
		}
	*/
	var iceServers []webrtc.ICEServer
	if err := json.Unmarshal([]byte(iceServersString), &iceServers); err != nil {
		log.Println(id, err)
		return
	}

	// for i := range iceServers {
	// 	log.Println(id, "ICEServer", i, iceServers[i])
	// }

	// Upgrade HTTP request to Websocket
	unsafeConn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print(id, " websocket upgrade error :", err)
		return
	}

	c := &threadSafeWriter{unsafeConn, sync.Mutex{}}

	// When this frame returns close the Websocket
	defer c.Close() //nolint

	config := webrtc.Configuration{
		ICEServers:         iceServers,
		ICETransportPolicy: webrtc.ICETransportPolicyAll,
	}

	// Create a new RTCPeerConnection
	//peerConnection, err := webrtc.NewPeerConnection(config)
	peerConnection, err := api.NewPeerConnection(config)

	if err != nil {
		panic(err)
	}
	log.Println(id, "New Peer Connection")

	// When this frame returns close the PeerConnection
	defer peerConnection.Close() //nolint

	//Accept one audio and one video track incoming
	for _, typ := range []webrtc.RTPCodecType{webrtc.RTPCodecTypeVideo, webrtc.RTPCodecTypeAudio} {
		if _, err := peerConnection.AddTransceiverFromKind(typ, webrtc.RTPTransceiverInit{
			Direction: webrtc.RTPTransceiverDirectionRecvonly,
		}); err != nil {
			log.Print(err)
			return
		}
	}

	// Add our new PeerConnection to global list
	listLock.Lock()
	peerConnections = append(peerConnections, peerConnectionState{peerConnection, c})
	listLock.Unlock()

	// Trickle ICE. Emit server candidate to client
	peerConnection.OnICECandidate(func(i *webrtc.ICECandidate) {
		log.Println(id, "OnICECandidate", i)

		if i == nil {
			return
		}

		candidateString, err := json.Marshal(i.ToJSON())
		if err != nil {
			log.Println(err)
			return
		}

		if writeErr := c.WriteJSON(&websocketMessage{
			Event: "candidate",
			Id:    id,
			Data:  string(candidateString),
		}); writeErr != nil {
			log.Println(writeErr)
		}
	})

	peerConnection.OnICEConnectionStateChange(func(connectionState webrtc.ICEConnectionState) {
		log.Println(id, "ICE Connection State has changed", connectionState)

		if connectionState == webrtc.ICEConnectionStateDisconnected {
			if err := peerConnection.Close(); err != nil {
				panic(err)
			}
			log.Println(id, "PeerConnection disconnected")
		} else if connectionState == webrtc.ICEConnectionStateConnected {
			log.Println(id, "PeerConnection connected")
		}
	})

	// If PeerConnection is closed remove it from global list
	peerConnection.OnConnectionStateChange(func(p webrtc.PeerConnectionState) {
		log.Println(id, "Connection State has changed", p)

		switch p {
		case webrtc.PeerConnectionStateFailed:
			if err := peerConnection.Close(); err != nil {
				log.Print(err)
			}
		case webrtc.PeerConnectionStateClosed:
		default:
		}
	})

	// add video track
	if _, err := peerConnection.AddTrack(outboundVideoTrack); err != nil {
		log.Println("AddTrack error")
		log.Println(err)
		return
	}

	log.Println(id, "track added")

	// Create an offer with ICE restart enabled
	offerOptions := webrtc.OfferOptions{
		ICERestart: true,
	}

	offer, err := peerConnection.CreateOffer(&offerOptions)
	if err != nil {
		log.Println("CreateOffer error")
		log.Println(err)
		return
	}

	log.Println(id, "offer created")

	if err = peerConnection.SetLocalDescription(offer); err != nil {

		log.Println(err)
		return
	}

	log.Println(id, "local description set")

	offerString, err := json.Marshal(offer)
	if err != nil {
		log.Println(err)
		return
	}

	log.Println(id, "offer marshalled")

	if err = c.WriteJSON(&websocketMessage{
		Event: "offer",
		Id:    id,
		Data:  string(offerString),
	}); err != nil {
		log.Println(err)
		return
	}

	log.Println(id, "offer sent")

	message := &websocketMessage{}
	for {
		_, raw, err := c.ReadMessage()
		if err != nil {
			log.Println(err)
			return
		} else if err := json.Unmarshal(raw, &message); err != nil {
			log.Println(err)
			return
		}

		switch message.Event {
		case "candidate":
			candidate := webrtc.ICECandidateInit{}
			if err := json.Unmarshal([]byte(message.Data), &candidate); err != nil {
				log.Println(err)
				return
			}

			if err := peerConnection.AddICECandidate(candidate); err != nil {
				log.Println(err)
				return
			}
			log.Println(id, "Added ICE Candidate")
		case "answer":
			answer := webrtc.SessionDescription{}
			if err := json.Unmarshal([]byte(message.Data), &answer); err != nil {
				log.Println(err)
				return
			}

			if err := peerConnection.SetRemoteDescription(answer); err != nil {
				log.Println(err)
				return
			}
			log.Println(id, "Set Answer")
		case "close": // Close the connection
			if err := peerConnection.Close(); err != nil {
				panic(err)
			}
			return
		}
	}
}

// Helper to make Gorilla Websockets threadsafe
type threadSafeWriter struct {
	*websocket.Conn
	sync.Mutex
}

func (t *threadSafeWriter) WriteJSON(v interface{}) error {
	t.Lock()
	defer t.Unlock()

	return t.Conn.WriteJSON(v)
}
